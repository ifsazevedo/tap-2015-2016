package AdviceExecutionTest;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectTest2 {
	
	@Before("adviceexecution() && within(AspectTest)")
	//This advice will be the first one to be executed: before the execution of any advice in AspectTest
	public void advice2() {
		System.out.println("Advice2");
	}
	

	
}
