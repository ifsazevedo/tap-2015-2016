package AdviceExecutionTest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectTest {
	
	@Before("call(* *.method1(..))")
	public void advice1() {
		System.out.println("Advice1");
	}
		
}
