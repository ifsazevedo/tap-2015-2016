package PL3.AnnotationsWithAttributes.TAP;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;


@Aspect
public class AspectAnnotationsWithAttributes {
	@Before("execution(@Critical * *.*(..)) && @annotation(s)")
	public void trace(JoinPoint jp, Critical s)   {
	System.out.println(s.level()+" : "+jp.getSignature());
	}

}
