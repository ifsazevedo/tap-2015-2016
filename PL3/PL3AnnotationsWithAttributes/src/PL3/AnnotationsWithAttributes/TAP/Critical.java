package PL3.AnnotationsWithAttributes.TAP;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Critical {
	int level();

}
