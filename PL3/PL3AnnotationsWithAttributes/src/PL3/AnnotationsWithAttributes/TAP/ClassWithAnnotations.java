package PL3.AnnotationsWithAttributes.TAP;

public class ClassWithAnnotations {
	
	@Critical(level=3)
	public void method1(){
		System.out.println("Very critical!");
	}
	
	@Critical(level=3)
	public void method2(){
		System.out.println("Very critical!");
	}
	
	@Critical(level=0)
	public void method3(){
		System.out.println("Not critical!");
	}

	public static void main(String[] args) {
		ClassWithAnnotations test1 =new ClassWithAnnotations();
		test1.method1();
		test1.method2();
		test1.method3();
		// TODO Auto-generated method stub

	}

}
