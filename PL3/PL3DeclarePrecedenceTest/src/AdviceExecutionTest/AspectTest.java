package AdviceExecutionTest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclarePrecedence;

@Aspect
//AspectTest before AspectTest2. It is simple to change the order
@DeclarePrecedence("AspectTest, AspectTest2")
public class AspectTest {
	
	@Before("call(* *.method1(..))")
	public void advice1() {
		System.out.println("Advice1");
	}
		
}
