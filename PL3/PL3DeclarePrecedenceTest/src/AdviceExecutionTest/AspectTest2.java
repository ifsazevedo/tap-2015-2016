package AdviceExecutionTest;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectTest2 {
	
	@Before("call(* *.method1(..))")
	public void advice2() {
		System.out.println("Advice2");
	}
	

	
}
